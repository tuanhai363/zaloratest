import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

@connect(({ app }) => {
	return {

	};
})

export default class Header extends Component {

  render () {
  	const  button = this.props.param.nickNameShow === '' ? 'React Twitter' : this.props.param.nickNameShow;

    return <View style={styles.container}>
      <View style={styles.headerLeft}>
				<Text style={{color:'white'}}>{button}</Text>
			</View>
			<View style={styles.containerCenter}>

			</View>
			<View style={styles.headerRight}>

			</View>
    </View>
  }
}

const styles = StyleSheet.create({
  container: {
		height: 40,
		backgroundColor:'#3f51b5',
		flexDirection:"row"
	},
	containerCenter: {
  	flex:1,
		backgroundColor: '#3b5998'
	},
	headerLeft: {
  	width: 100,
		justifyContent:'center',
		alignItems:'center'
	},
	headerRight :{
  	width: 100
	}

});