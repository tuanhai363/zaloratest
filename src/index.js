import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { utils, RuuiProvider, Button, Tooltip } from 'react-universal-ui';
import { Provider } from 'react-redux';
import { MemoryRouter, StaticRouter } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';

import routes from "./routes";
import {store} from "./store";


function getRouterAndProps(props) {
	if (utils.isServer) {
		return {
			component: StaticRouter,
			props: {
				location: props.ssrLocation,
				context: props.ssrContext,
			},
		};
	} else if (utils.isWeb) {
		return {
			component: BrowserRouter,
			props: {},
		};
	} else {
		return {
			component: MemoryRouter,
			props: {},
		};
	}
}

export default function AppContainer(props) {
	const routerAndProps = getRouterAndProps(props),
		Router = routerAndProps.component,
		routerProps = routerAndProps.props;

	return <RuuiProvider>
		<Provider store={store}>
			<Router {...routerProps}>
				{renderRoutes(routes)}
			</Router>
		</Provider>
		<Tooltip/>
	</RuuiProvider>;
}

