import * as Actions from '../actions';

export function nickNameLogin (nickName) {
	return { type: Actions.NickNameLogin, nickName };
}