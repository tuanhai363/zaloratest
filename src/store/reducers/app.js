import * as Actions from '../actions';

const initialState = {
	nickName: ''
};

export default (state = initialState, action) => {
	switch (action.type) {
		case Actions.NickNameLogin:
			return { ...state, nickName: action.nickName };
		default:
			return state;
	}
}