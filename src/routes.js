import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { renderRoutes } from 'react-router-config';
import { connect } from 'react-redux';

import HomeScreen from "./scenes/home";
import LoginForm from "./scenes/login";
import NotFoundScene from "./scenes/notFound";
import Header from "./header";

type Props = {
	dispatch?: Function,
	route: Route,
	location?: Object,
	history?: Object,
};

@connect(({ app }) => {
	return {
		nickNameShow: app.nickName
	};
})

class Layout extends Component {
  props: Props;
  render () {
    return <View style={styles.container}>
      <Header param={this.props}/>
      {renderRoutes(this.props.route.routes, this.props)}
    </View>
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default [{
  component: Layout,
  routes: [{
		path:'/',
		exact: true,
		component: LoginForm
	}, {
		path:'/home/:user',
		exact: true,
		component: HomeScreen
	}, {
  	component: NotFoundScene
  }],
}];