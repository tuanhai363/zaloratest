import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import {connect} from "react-redux";


@connect(({ app }) => {
	return {

	};
})
export default class ContentShow extends Component {
	props:Props;
	constructor(props) {
		super(props);
		this.state = {
		}
	}
  render () {
		const nickName = this.props.action.param.nickNameShow === '' ? 'Content Show' : this.props.action.param.nickNameShow;
    return <View style={styles.container}>
      <Text style={styles.textTitle}>{nickName}</Text>
			<ScrollView
				contentContainerStyle={styles.containerContent}>
				{this.props.param.map((item, index)=>{
					return this.renderPostsInfo(item, index)
				})}
			</ScrollView>
    </View>
  }
  renderPostsInfo = (item, index) => {
		return <View key={index} style={{ paddingVertical:5 }}>
			<Text style={styles.textStyle}>{item.message}</Text>
		</View>
	}
}


const styles = StyleSheet.create({
	container: {
	  flex:1,
  },
	containerContent: {
		flex:1,
		zIndex: 5,
		width: '100%',
		justifyContent:'center',
		alignItems:'flex-start'
	},
	textTitle :{
		fontSize: 16,
		color: '#ff6625',
		paddingVertical: 5,
		fontWeight: 'bold'
	},
	textStyle:{
		fontSize: 14,
		color: '#1a1a1a'
	}

});