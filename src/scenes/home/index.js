import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import FromAction from "./actionTweet";
import { connect } from 'react-redux';

@connect(({ app }) => {
	return {

	};
})


export default class HomeScreen extends Component {
  render () {
    return <View style={styles.container}>
			<FromAction param={this.props}/>
    </View>
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
});