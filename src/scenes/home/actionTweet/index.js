import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import ContentShow from "../contentShow/index";

export default class FromAction extends Component {
	constructor(props) {
		super(props);
		this.state = {
			content:'',
			messageParam:[...postsInfo],
			arrayMess:[],
		}
	}
	onPress = () => {
		const textInfo = this.state.content;
		let messageArray = [...this.state.messageParam];

		if (textInfo !== '') {
			const n = Math.ceil(textInfo.length / 49);
			console.log(n);
			for (i = 0; i < n; i++) {
				messageArray.push({
					message: [`[${i + 1}/${n}]`, this.state.content.slice(49 * i, 49*(i + 1))].join(' '),
					user:'true'
				});
			}
		}
		this.setState({arrayMess:messageArray, messageParam: messageArray, content: ''});
	};

  render () {
    return <View style={styles.container}>
      <View style={{flex:1}}/>
			<View style={styles.childrenContent}>
				<View style={styles.containerChildren}>
					<Text style={styles.titleForm}>New Message To Day </Text>
					<View style={{marginTop: 10}}>
						<TextInput
							style={styles.containerInput}
							onChangeText={(content) => this.setState({content})}
							placeholder={'How are you today???'}
							placeholderTextColor={'#9c9c9c'}
							value={this.state.content}
							multiline
						/>
					</View>
					<View style={styles.containerButton}>
						<TouchableOpacity
							style={styles.buttonTweet}
							onPress={this.onPress}
						>
							<Text style={{color:"#ffffff"}}>Tweet</Text>
						</TouchableOpacity>
					</View>
				</View>
				<View style={{marginTop: 10}}>
					<ContentShow param={this.state.arrayMess} action={this.props}/>
				</View>
			</View>
			<View style={{flex:1}}/>
    </View>
  }

}

const postsInfo = [

];

const styles = StyleSheet.create({
	container: {
	  flex:1,
		flexDirection:'row',
  },
	childrenContent:{
		flex:1
	},
	containerChildren :{
		flex:1,
		paddingHorizontal: 10,
		paddingVertical: 10,
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 1,
		shadowColor: '#000',
		maxHeight: 160,
		borderRadius:3,
	},
	containerInput: {
		minHeight: 80,
		borderColor: 'gray',
		borderBottomWidth: 1,
		borderBottomColor:'#797979',
		minWidth: 350,
		alignItems:'flex-start'
	},
	buttonTweet: {
		alignItems: 'center',
		justifyContent:'center',
		backgroundColor: '#4266b2',
		padding: 5,
		width: 80,
		height: 30,
		borderRadius: 3
	},
	containerButton: {
		alignItems:'flex-end',
		marginTop: 5
	},
	titleForm: {
		color:'#000085',
		fontSize: 14,
		fontWeight: 'bold',
	}
});