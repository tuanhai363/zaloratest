import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import { connect } from 'react-redux';
import {store} from "../../store";
import * as appActions from '../../store/action/app';

@connect(({ app }) => {
	return {
		nickNameShow: app.nickName
	};
})

export default class LoginForm extends Component {
	constructor(props) {
		super(props);
		this.state = { text: '' };
	}

  render () {
    return <View style={styles.container}>
			<View style={styles.inputContainer}>
				<TextInput
					style={{height: 40, marginTop: 10, borderColor: '#f2c775', borderWidth: 1, borderRadius: 5, paddingLeft: 5 }}
					onChangeText={(text) => this.setState({text})}
					value = {this.state.text}
					placeholder = {'Nick name'}
				/>
				<View style={styles.containerButton}>
					<TouchableOpacity style={styles.buttonLogin}
														onPress={() => {
															this.props.dispatch(appActions.nickNameLogin(this.state.text));
																this.props.history.push(`/home/${this.state.text}`)
														}}>
						<Text style={{color: '#ffffff'}}>Login</Text>
					</TouchableOpacity>
				</View>
			</View>
    </View>
  }
}

const styles = StyleSheet.create({
	container:{
		flex: 1,
		alignItems: 'center'
	},
	inputContainer:{
		flex: 1,
		width:'100%',
		paddingHorizontal: 40,
		paddingTop: 15,
		maxWidth: '30%',
	},
	buttonLogin :{
		height: 35,
		width: 120,
		backgroundColor: '#2f2242',
		justifyContent:'center',
		alignItems:'center',
		marginTop: 30,
	},
	containerButton: {
		flex:1,
		alignItems:'flex-end'
	}
});